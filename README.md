24-6-20 9:01 AM
Farhan Sadik 
# Fragment with AppBarLayout, TabLayout, Toolbar, ViewPager

### 1. Add Library 

> /app/build.gradle

```
implementation "androidx.coordinatorlayout:coordinatorlayout:1.1.0"
implementation 'com.google.android.material:material:1.1.0'
```
> /build.gradle

```
classpath "com.android.support:design:28.0.0"
```

### 0. Remove Action Bar
If you want then you can remove action bar. from `styles.xml` and just edit it like this. 
```
<style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">
```

### 2. Working with `activity_main.xml`
```
<androidx.coordinatorlayout.widget.CoordinatorLayout
    android:layout_height="match_parent"
    android:layout_width="match_parent"
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <!--we will use Appbar layout to hold toolbar and tablayout-->
    <com.google.android.material.appbar.AppBarLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <androidx.appcompat.widget.Toolbar
            android:layout_width="match_parent"
            android:layout_height="?attr/actionBarSize"
            app:title="Leco Miniko"
            app:titleTextColor="#fff">
        </androidx.appcompat.widget.Toolbar>

        <com.google.android.material.tabs.TabLayout
            android:id="@+id/my_tabs"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="#283593"
            app:tabTextColor="#c5cae9"
            app:tabSelectedTextColor="#fff">
        </com.google.android.material.tabs.TabLayout>

    </com.google.android.material.appbar.AppBarLayout>

    <!--to get the views according to tab-->
    <androidx.viewpager.widget.ViewPager
        android:id="@+id/my_view_pager"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
    </androidx.viewpager.widget.ViewPager>

</androidx.coordinatorlayout.widget.CoordinatorLayout>
```

### 3. Create a new Layout `fragment_one_layout.xml`
```
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical" android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:gravity="center">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="30sp"
        android:text="@string/f1_text_view"
        android:gravity="center"/>

    <Button
        android:id="@+id/my_site_button"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="@string/go_to_the_site"
        android:layout_margin="20dp"
        android:padding="15dp"
        android:background="#00aee5"
        android:textColor="#fff"
        android:textSize="19sp"/>

</LinearLayout>
```

### 4. Create 2nd  Layout `fragment_two_layout.xml`
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical" android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:gravity="center">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="30sp"
        android:text="@string/f2_text_view"
        android:gravity="center"/>

</LinearLayout>
```

### 5. Create 3rd Layout `fragment_three_layout.xml`
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical" android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:gravity="center">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="30sp"
        android:text="@string/f3_text_view"
        android:gravity="center"/>

</LinearLayout>
```

### 6.  Create a new Class `FragmentOne`
```
public class FragmentOne extends Fragment {

    View v;
    Button b;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_one_layout , container, false);
        
        // if you use a button
        b = (Button) v.findViewById(R.id.my_site_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Clicked on button", Toast.LENGTH_LONG).show();
            }
        });

        return v;

    }
}
```

### 7.  Create a new Class `FragmentTwo`
```
View v2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v2 = inflater.inflate(R.layout.fragment_two_layout, container, false);

        return v2;

    }
```

### 8.  Create a new Class `FragmentThree`
```
View v3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v3 = inflater.inflate(R.layout.fragment_three_layout , container, false);

        return v3;

    }
```

### Working in MainActivity
```
private TabLayout my_tl;
    private ViewPager my_vp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        my_vp = findViewById(R.id.my_view_pager);
        my_tl = findViewById(R.id.my_tabs);

        setUpMyViewPager(my_vp);
        my_tl.setupWithViewPager(my_vp);

    }

    void setUpMyViewPager(ViewPager vp){

        ViewPagerAdapter vpa = new ViewPagerAdapter(getSupportFragmentManager());
        vpa.addMyFragment(new FragmentOne(), "AndroidLIme");
        vpa.addMyFragment(new FragmentTwo(), "M Rubel");
        vpa.addMyFragment(new FragmentThree(), "ImRubel");

        vp.setAdapter(vpa);

    }


    static class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> my_list = new ArrayList<Fragment>();
        private final List<String> my_titles = new ArrayList<String>();

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return my_list.get(position);
        }

        @Override
        public int getCount() {
            return my_list.size();
        }

        void addMyFragment(Fragment f, String title){
            my_list.add(f);
            my_titles.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return my_titles.get(position);
        }

    }
```
source code ~ [https://gitlab.com/android-practice1/android-fragment-combination](https://gitlab.com/android-practice1/android-fragment-combination)